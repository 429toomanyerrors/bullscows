from random import randint



# Generates a list of four, unique, single-digit numbers
def generate_secret():
    # TODO:
    # You need to delete the word "pass" and write your
    # function, here.
    seclist=[]
    while len(seclist) <4:
        n = randint(0,9)
        if n not in seclist:
            seclist.append(n)
    return seclist
def parsenum(s):
    l=[]
    for n in s:
        number = int(n)
        l.append(number)
    return l
def checkexact(firstl, secondl):
    count = 0
    for a, b in zip(firstl, secondl):
        if a == b:
            count = count + 1
    return count
def checkloose (firstl, secondl):
    count = 0
    for entry in firstl:
        if entry in secondl:
            count= count + 1
    return count

def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    number_of_guesses = 5

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        # TODO:
        # while the length of their response is not equal
        # to 4,
        #   tell them they must enter a four-digit number
        #   prompt them for the input, again
        while len(guess) != 4:
            print("Invalid entry, please enter a non repeating 4 digit number")
            guess = input(prompt)

        # TODO:
        # write a function and call it here to:
        # convert the string in the variable guess to a list
        # of four single-digit numbers
        listedguess = parsenum(guess)
        # TODO:
        # write a function and call it here to:
        # count the number of exact matches between the
        # converted guess and the secret
        exactcount = checkexact(listedguess, secret)
        print("bulls", exactcount)
        # TODO:
        # write a function and call it here to:
        # count the number of common entries between the
        # converted guess and the secret
        loosecount= checkloose(listedguess, secret)
        print("cows",loosecount)
        # TODO:
        # if the number of exact matches is four, then
        # the player has correctly guessed! tell them
        # so and return from the function.
        if exactcount == 4:
            print("You guessed correctly")
            return
        # TODO:
        # report the number of "bulls" (exact matches)
        # TODO:
        # report the number of "cows" (common entries - exact matches)

    # TODO:
    # If they don't guess it in 20 tries, tell
    # them what the secret was.
    print("The number was", secret)

# Runs the game
def run():
    response= input("Do you want to play a game? y/n")

    # TODO: Delete the word pass, below, and have this function:
    # Call the play_game function
    # Ask if they want to play again
    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again
    while response == "y":
        play_game()
        response = input("Do you want to play again? y/n")

if __name__ == "__main__":
    run()
